<?php foreach($comments as $comment): ?>
	<div class="panel panel-success">
		<div class="panel-heading">
			<?php if(Auth::instance()->logged_in("admin")): ?>
			<a href="<?php echo URL::site('admin/main/delete_comment/'.$comment->id); ?>" class="close" onclick="return confirm('Вы уверены, что хотите удалить?')">&times;</a>
			<?php endif; ?>
			<?php echo $comment->user; ?> | <?php $dt_elements=explode(' ',$comment->created_at); echo $dt_elements[0];?><br />
		</div>
		<div class="panel-body">
			<?php echo $comment->message; ?><br />
		</div>
	</div>
	<br />
<?php endforeach; ?>
<?php if(Auth::instance()->logged_in()): ?>
<form action="" method="POST">
	<table>	
		<tr>
			<td>
				<label>Оставить комментарий</label><br />
				<p><strong style="color: #f00;"><?php if(isset($errors['message'])); echo $errors['message']?></strong></p>
				<textarea name="message" cols="60" rows="5"><?php echo HTML::chars(Arr::get($_POST, 'message')); ?></textarea>
			</td>
		</tr>
		<tr>			
			<td>
				<input type="submit" name="send" value="Отправить"/>
			</td>
		</tr>
	</table>
</form>
<?php else:?>
	<p>Чтобы оставить комментарий <i><a href="<?php echo URL::site("/auth"); ?>">войдите в систему</a></i></p>
<?php endif; ?>
