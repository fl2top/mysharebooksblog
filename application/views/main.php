<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php echo $description; ?>">
	<meta name="keywords" content="<?php echo $keywords; ?>">
	<title><?php echo $title ?></title>
	<link rel="icon" type="image/png" href="public/images/favicon.png" >
	<?php foreach($styles as $style):?>
	<?php echo HTML::style($style); ?>
	<?php endforeach?>
	<?php foreach($scripts as $script):?>
	<?php echo HTML::script($script); ?>
	<?php endforeach?>
</head>
<body>
	 <div class="blog-masthead">
		<div class="container">
			<nav class="blog-nav">
			<a <?php if($_SERVER["REQUEST_URI"] == URL::base()) { echo "class='blog-nav-item active'"; } else {  echo "class='blog-nav-item'";}?> href="<?php echo URL::site(); ?>" >Главная</a>
			<a <?php if($_SERVER["REQUEST_URI"] == URL::base()."list/listall") { echo "class='blog-nav-item active'"; } else {  echo "class='blog-nav-item'";}?> href="<?php echo URL::site("list/listall"); ?>">Список статей</a>
			<a <?php if($_SERVER["REQUEST_URI"] == URL::base()."myself") { echo "class='blog-nav-item active'"; } else {  echo "class='blog-nav-item'";}?> href="<?php echo URL::site("myself"); ?>">Автор</a>
			<a <?php if($_SERVER["REQUEST_URI"] == URL::base()."about") { echo "class='blog-nav-item active'"; } else {  echo "class='blog-nav-item'";}?> href="<?php echo URL::site("about"); ?>">Зачем?</a>
			<?php if(Auth::instance()->logged_in("admin")): ?>
			<a <?php if($_SERVER["REQUEST_URI"] == URL::base()."admin") { echo "class='blog-nav-item active'"; } else {  echo "class='blog-nav-item'";}?> href="<?php echo URL::site("admin"); ?>">Добавить статью</a>
			<a <?php if($_SERVER["REQUEST_URI"] == URL::base()."list/users") { echo "class='blog-nav-item active'"; } else {  echo "class='blog-nav-item'";}?> href="<?php echo URL::site("list/users"); ?>">Список пользователей</a>
			<?php endif; ?>
			<?php if(!Auth::instance()->logged_in()): ?>
			<a class="blog-nav-item" href="<?php echo URL::site("auth"); ?>" >Авторизация</a>
			<a class="blog-nav-item" href="<?php echo URL::site("auth/register"); ?>" >Регистрация</a>
			<?php endif; ?>
			<?php if(Auth::instance()->logged_in()): ?>
			<a class="blog-nav-item" href="<?php echo URL::site("auth/logout"); ?>">Выйти</a>
			<?php endif; ?>
			</nav>
		</div>
    </div>
    <div class="container">
		<div class="blog-header">
			<h1 class="blog-title">My ShareBooks Blog</h1>
			<p class="lead blog-description">Многие люди читают только для того, чтобы иметь право не думать.</p>
		</div>
		<div class="row">
			<div class="col-sm-8 blog-main">
				<?php echo $content; ?>
			</div><!-- /.blog-main -->
			<div class="col-sm-3 col-sm-offset-1 blog-sidebar">
				<div class="sidebar-module sidebar-module-inset">
					<h4>О блоге</h4>
					<p>Здесь Вы можете прочесть <em>мое мнение</em> о прочитанных книгах, <em>высказать свое</em>... или же оставить все как есть.</p>
				</div>
				<?php echo $archive; ?>
				<div class="sidebar-module">
					<!--noindex-->
					<h4>Контакты</h4>
					<ol class="list-unstyled">
					  <li><a rel="nofollow" href="https://vk.com/id78990811">ВКонтакте</a></li>
					  <li><a rel="nofollow" href="http://readrate.com/rus/users/4730">Readrate</a></li>
					</ol>
					<!--/noindex-->
				</div>
			</div><!-- /.blog-sidebar -->
		</div><!-- /.row -->
    </div><!-- /.container -->
    <footer class="blog-footer">
		<p>My ShareBooks Blog &copy; by <em>Mitrakhovich Alex</em></p>
		<p>
			<a id="back-top" href="#">Вернуться в начало</a>
		</p>
    </footer>

	 <script>
		 $(document).ready(function() {
			 $("#back-top").click(function () {
				 $("body,html").animate({
					 scrollTop: 0
				 }, 800);
				 return false;
			 });
		 });
	 </script>
	
    <!-- Bootstrap core JavaScript
    ================================================== -->
	<?php foreach($last_scripts as $script):?>
	<?php echo HTML::script($script); ?>
	<?php endforeach?>

	<!-- Google Analytics
	================================================== -->
	 <script>
		 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		 ga('create', 'UA-66486108-1', 'auto');
		 ga('send', 'pageview');
	 </script>
</body>
</html>