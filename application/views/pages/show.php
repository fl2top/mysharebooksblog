<div class="blog-post">
<?php foreach($articles as $article): ?>
	<h2 class="blog-post-title"><?php echo $article->writer; ?> - <?php echo $article->title; ?></h2>
	<p class="blog-post-meta"><?php echo $article->date; ?> | <a href="myself"><?php echo $article->author; ?></a></p>
	<p><?php echo $article->content_short; ?></p>
	<a href="<?php echo URL::site('articles/'.$article->id.'-'.$article->alt_title); ?>">Подробнее</a> 
	<hr />
<?php endforeach; ?>
</div><!-- /.blog-post -->
<?php echo $pagination; ?>
