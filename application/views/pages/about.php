<div class="blog-post">
	<h2 class="blog-post-title">О блоге</h2>
	<p class="blog-post-meta"></p>
	<p>
		Как говорит Википедия: "Блог (англ. blog, от web log — интернет-журнал событий, интернет-дневник, онлайн-дневник) 
		— веб-сайт, основное содержимое которого — регулярно добавляемые записи, содержащие текст, изображения или мультимедиа". 
		Вот я и не стану отступать от общепринятых норм и полностью последую вышеизложенному определению. Однако регулярность 
		появления новых постов будет зависеть от свободного времени и, что разумеется, моего настроения.
	</p>
	<p>
		А теперь что касается тематики и самого блога. Хоть в кратком описании все и так доступно изложено, пару слов я все же добавлю.
		Прежде всего необходимо подчеркнуть, что <em><u>все написанное в постах исключительно мое собственное мнение</u></em>. А, как говориться,
		сколько людей, столько и мнений. То есть у вас есть три пути, если Вы все же решите прочесть хоть одну статью: <br />
		<ul>
			<li>Согласиться с моим мнением.</li>
			<li>Высказать свое.</li>
			<li>Просто закрыть вкладку (верно, нечего эти бредни читать).</li>
		</ul>
		Разумеется желательнее первые два, однако и третий не так уж плох, в особенности для меня. Почему? Потому что лучше так, чем потом 
		удалять комментарии с нецензурной лексикой и банить пользователей. Здесь же подразумевается, что посещающие этот сайт люди понимают
		за что пользователи попадают в черный список (а бан это и подразумевает) без возможности восстановления. Так что будьте благоразумны 
		и старайтесь предварительно фильтровать то, что набираете на клавиатуре.	
	</p>
	<p>
		<strong><em>P.S.</em></strong> Блог все еще находится в процессе разработки. По мере развития проекта будет наращиваться его функционал. 
		Спасибо за понимание.
	</p>
	<p>
		С уважением,
		<br />
		Алексей Митрахович
	</p>
</div><!-- /.blog-post -->