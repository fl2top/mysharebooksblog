<div class="container">
	<form class="form-signin" action="" method="POST">
		<h2 class="form-signin-heading">Регистрация</h2>
		<label for="inputName" class="sr-only">Имя</label>
			<input type="name"  name="username" id="inputEmail" class="form-control" placeholder="Имя" required autofocus value="<?php echo HTML::chars(Arr::get($_POST, 'username')); ?>">
		<label for="inputEmail" class="sr-only">E-mail</label>
			<input type="email"  name="email" id="inputEmail" class="form-control" placeholder="Email" required autofocus value="<?php echo HTML::chars(Arr::get($_POST, 'email')); ?>"> 
		<label for="inputPassword" class="sr-only">Пароль</label>
			<input type="password" name="password" id="inputPassword" class="form-control" placeholder="Пароль" style="margin:0; border-radius:0;" required value="<?php echo HTML::chars(Arr::get($_POST, 'password')); ?>">
		<label for="inputPassword" class="sr-only">Повторите пароль</label>
			<input type="password" name="password_confirm" id="inputPassword" class="form-control" placeholder="Повторите пароль" required value="<?php echo HTML::chars(Arr::get($_POST, 'password_confirm')); ?>">
		<input type="submit" name="send"  class="btn btn-lg btn-primary btn-block" value="Зарегистрироваться"/>
		<?php if($errors):?>
            <?php foreach($errors as $error):?>
                <strong style="color: red; text-align: center; display: block;" > <?php echo $error;?></strong>
            <?php endforeach; ?>
		<?php endif; ?>
	</form>
</div> <!-- /container -->