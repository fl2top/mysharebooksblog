<div class="blog-post">
<?php if($article): ?>
	<h2 class="blog-post-title"><?php echo $article->title; ?></h2>
	<p class="blog-post-meta">Дата публикации: <?php echo $article->date; ?></p>
	<p class="blog-post-meta">Автор: <a href="/myself"><?php echo $article->author; ?></a></p>
	<p><?php echo $article->content_full; ?></p>
	<?php if(Auth::instance()->logged_in('admin')): ?>
	<a href="<?php echo URL::site('admin/main/edit/'.$article->id); ?>">Редактировать</a> |
	<a href="<?php echo URL::site('admin/main/delete/'.$article->id); ?>" onclick="return confirm('Вы уверены, что хотите удалить?')" >Удалить</a>
	<?php endif; ?>
	<hr />
	<?php echo $comments; ?>
<?php else: ?>
	<p>Статья не найдена или не существует!</p>
<?php endif; ?>
</div><!-- /.blog-post -->