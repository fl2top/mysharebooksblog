<div class="container">
	<form class="form-signin" action="" method="POST">
		<h2 class="form-signin-heading">Авторизация</h2>
		<label for="inputEmail" class="sr-only">E-mail</label>
			<input type="email"  name="email" id="inputEmail" class="form-control" placeholder="Email" required autofocus value="<?php echo HTML::chars(Arr::get($_POST, 'email')); ?>"> 
		<label for="inputPassword" class="sr-only">Пароль</label>
			<input type="password" name="password" id="inputPassword" class="form-control" placeholder="Пароль" required value="<?php echo HTML::chars(Arr::get($_POST, 'password')); ?>">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="remember" value="true"> Запомнить меня | <a  href="<?php echo URL::site("/auth/register"); ?>">Регистрация</a>
            </label>
        </div>
		<input type="submit" name="send"  class="btn btn-lg btn-primary btn-block" value="Подтвердить"/>
		<?php if($errors):?>
			<strong style="color: red; text-align: center; display: block;" > <?=$errors[0];?></strong>
		<?php endif; ?>

	</form>
</div> <!-- /container -->