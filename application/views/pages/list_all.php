<h2>Список всех статей</h2>
	<table class="table table-hover">	
		<tr  class="info">
			<th>
				Писатель 
			</th>
			<th>
				Название 
			</th>
			<th>
				Дата публикации
			</th>
			<?php if(Auth::instance()->logged_in("admin")): ?>
			<th>
				Действие
			</th>
			<?php endif;?>
		</tr>
	<?php foreach($lists as $list): ?>
		<tr>
			<td>
				<?php echo $list->writer; ?>
			</td>
			<td>
				<a href="<?php echo URL::site('articles/'.$list->id.'-'.$list->alt_title); ?>" ><?php echo $list->title; ?></a>
			</td>
			<td>
				<?php echo $list->date; ?>
			</td>
			<?php if(Auth::instance()->logged_in("admin")): ?>
			<td>
				<a href="<?php echo URL::site('admin/main/edit/'.$list->id); ?>">Редактировать</a>
				<a href="<?php echo URL::site('admin/main/delete/'.$list->id); ?>" onclick="return confirm('Вы уверены, что хотите удалить?')">Удалить</a>
			</td>
			<?php endif;?>
		</tr>
	<?php endforeach; ?>
	</table>
