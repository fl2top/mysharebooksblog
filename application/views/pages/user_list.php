<h2>Список пользователей</h2>
	<table class="table table-hover">	
		<tr  class="info">
			<th>
				ID 
			</th>
			<th>
				Email 
			</th>
			<th>
				Username
			</th>
			<th>
				Action
			</th>
		</tr>
	<?php foreach($lists as $list): ?>
		<tr>
			<td>
				<?php echo $list->id; ?>
			</td>
			<td>
				<?php echo $list->email; ?>
			</td>
			<td>
				<?php echo $list->username; ?>
			</td>
			<td>
				<a href="<?php echo URL::site('admin/main/delete_user/'.$list->id); ?>" onclick="return confirm('Вы уверены, что хотите удалить?')" >Удалить</a>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
