<h2>Добавление статьи</h2>
<hr />
<form action="" method="POST">
	<table>	
		<tr>
			<td>
				<label>Автор произведения</label><br />
				<input type="text" name="writer" value="<?php echo HTML::chars(Arr::get($_POST, 'writer')); ?>"/> 
			</td>
			<td>
				<strong style="color: #f00;"><?php if(isset($errors['writer'])); echo $errors['writer']?></strong>
			</td>
		</tr>
		<tr>
			<td>
				<label>Название произведения</label><br />
				<input type="text" name="title" value="<?php echo HTML::chars(Arr::get($_POST, 'title')); ?>"/> 
			</td>
			<td>
				<strong style="color: #f00;"><?php if(isset($errors['title'])); echo $errors['title']?></strong>
			</td>
		</tr>
		<tr>
			<td>
				<label>URL статьи</label><br />
				<input type="text" name="alt_title" value="<?php echo HTML::chars(Arr::get($_POST, 'alt_title')); ?>"/> 
			</td>
			<td>
				<strong style="color: #f00;"><?php if(isset($errors['title'])); echo $errors['title']?></strong>
			</td>
		</tr>
		<tr>
			<td>
				<label>Аннотация</label><br />
				<textarea name="content_short" cols="60" rows="5" id="editor1" class="ckeditor"><?php echo HTML::chars(Arr::get($_POST, 'content_short')); ?></textarea>
			</td>
			<td>
				<strong style="color: #f00;"><?php if(isset($errors['content_short'])); echo $errors['content_short']?></strong>
			</td>
		</tr>
		<tr>
			<td>
				<label>Текст статьи</label><br />
				<textarea name="content_full" cols="60" rows="5" id="editor1" class="ckeditor"><?php echo HTML::chars(Arr::get($_POST, 'content_full')); ?></textarea>
			</td>
			<td>
				<strong style="color: #f00;"><?php if(isset($errors['content_full'])); echo $errors['content_full']?></strong>
			</td>
		</tr>
		<tr>			
			<td>
				<input type="submit" name="send" value="Добавить статью"/>
			</td>
		</tr>
	</table>
</form>
