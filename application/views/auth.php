<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>My ShareBooks Blog</title>
	<?=HTML::style("public/css/bootstrap.min.css");?>
	<?=HTML::style("public/css/signin.css");?>
</head>
<body>
    <?php echo $content; ?>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<?=HTML::script("public/js/workaround.js");?>
  </body>
</html>
