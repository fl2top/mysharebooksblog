<?php
	defined("SYSPATH") or die("No direct script access.");
	
	abstract class Controller_Common extends Controller_Template {
		
		public $template="main";
		
		public function before() {
			
			parent::before();
			
			$writers=ORM::factory('article')
				->group_by('writer')
				->find_all();
				
			$archive=View::factory('pages/archive')
					->bind('writers',$writers);
					
			View::set_global("title","My ShareBooks Blog");
			View::set_global("description","My ShareBooks Blog - блог о прочитанных книгах, где можно оставить свой отзыв");
			View::set_global("keywords","My ShareBooks Blog, Мнение о книгах и обсуждение, Книги, Алексей Митрахович, Блог о книгах");
			$this->template->content="";
			$this->template->archive=$archive;
			$this->template->styles=array("public/css/bootstrap.min.css","public/css/blog.css");
			$this->template->scripts=array("public/js/ckeditor/ckeditor.js","public/js/jquery.min.js");
			$this->template->last_scripts=array("public/js/bootstrap.min.js","public/js/workaround.js");
		}
		
	}
?>