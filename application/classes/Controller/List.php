<?php defined("SYSPATH") or die("No direct script access.");

	class Controller_List extends Controller_Common {
		
		public function action_index($id='') 
		{
			
			$writer=$this->request->param('writer');
			
			$lists=ORM::factory('article')
				->where('writer','=',$writer)
				->order_by('date', 'DESC')
				->find_all();
				
			$content=View::factory('pages/writer_list')
					->bind('lists',$lists);
					
			$this->template->content=$content;
		}
		
		public function action_listall() 
		{
			
			$lists=ORM::factory('article')
				->order_by('date', 'DESC')
				->find_all();
				
			$content=View::factory('pages/list_all')
					->bind('lists',$lists);
					
			$this->template->content=$content;
		}
		
		public function action_users() 
		{
			if(!Auth::instance()->logged_in("admin")) {
				$this->redirect("auth");
			}
			$lists=ORM::factory('user')
				->order_by('id')
				->find_all();
				
			$content=View::factory('pages/user_list')
					->bind('lists',$lists);
					
			$this->template->content=$content;
		}	
	}