<?php
	defined("SYSPATH") or die("No direct script access");
	
	class Controller_Comments extends Controller {
		
		public function action_index() 
		{
			if($this->request->is_initial()) 
			{
				HTTP::redirect(URL::site(""));
			}
			
			$article_id=$this->request->param("id");
					
			if($_POST) {
				$_POST = Arr::map('trim', $_POST);	
				$message=Arr::get($_POST,"message");
			 
				try
				{
					$user=Auth::instance()->get_user();
					$add=ORM::factory('comment');
					$add->message=$message;
					$add->article_id=$article_id;
					$add->user_id=$user->id;
					$add->user=$user->username;
					$add->created_at=date('y-m-d H:i:s');
					$add->save();
			 
					URL::site(Request::detect_uri());							
			 
				}
				catch(ORM_Validation_Exception $e)
				{
					$errors = $e -> errors('comment');
				}
			}
					
			$writers=ORM::factory('article',$article_id);

            $comments=$writers->comments
                ->order_by("created_at", 'ASC')
                ->find_all();

					
			$content=View::factory("/comments/show")
					->bind("comments",$comments)
					->bind("writers",$writers)
					->bind("errors",$errors);
			$this->response->body($content);
		}
	}