<?php defined("SYSPATH") or die("No direct script access");

	class Controller_Auth extends Controller_Template {
		
		public $template="auth";
		
		public function before() 
		{
			parent::before();
		}
		
		public function action_index()
		{
			if($_POST)
			{
				$_POST = Arr::map('trim', $_POST);	
				$data=Arr::extract($_POST,array(
									"email",
									"password",
                                    "remember"
									)
				);
                $remember = array_key_exists('remember', $this->request->post()) ? (bool) $this->request->post('remember') : FALSE;
				$success=Auth::instance()->login($data["email"],$data["password"],$remember);
				if($success) {
					$this->redirect("/");
				}	
				else {
					$errors=array("Ошибка входа! Проверьте правильность введенных данных!");
				}
			}
			$content=View::factory("pages/auth_main")
				->bind("errors",$errors);
			$this->template->content=$content;
		}
		
		public function action_register() 
		{
			if($_POST) {
				$data=Arr::extract($_POST,array(
									"username",
									"email",
									"password",
									"password_confirm"
								)
				);
				$user=ORM::factory("user");
				try 
				{
					$user->create_user($data, array(
										"username",
										"email",
										"password",
										)			
					);
					$role=ORM::factory("Role")
						->where("name", "=", "login")
						->find();
					$user->add("roles", $role);
                    mail($_POST["email"], "=?utf-8?B?".base64_encode("Registration on the site MyShareBooks Blog")."?=",
                        "You have been registered on the site mysharebooksblog.besaba.com <br />Your login - ".$_POST["username"].
                        "<br />Your password - ".$_POST["password"]." <br />Thank you for registration!", "From: registration@mysharebooksblog.besaba.com \r\nContent-Type: text/html; charset=UTF-8");
					$this->redirect("auth");
				} 
				catch(ORM_Validation_Exception $e) 
				{
					$errors=$e->errors("validation");
                    if( Arr::get($errors, '_external', array()) != null) {
                        $errors = Arr::merge($errors, Arr::get($errors, '_external'));
                        unset($errors['_external']);
                    }

				}
			}
			$content=View::factory("pages/reg")
				->bind("errors", $errors);
			$this->template->content=$content;
		}
		
		public function action_logout() {
			Auth::instance()->logout();
			$this->redirect("/");
		} 
	}