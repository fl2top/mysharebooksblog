<?php
	defined("SYSPATH") or die("No direct script access");
	
	class Controller_Articles extends Controller_Common {
		
		public function before() 
		{
			parent::before();
		}
		
		public function action_article($id='') 
		{
			$id=$this->request->param("id");
			
			$article=ORM::factory('article')
					->where('id','=',$id)
					->find();
			
			$content=View::factory("/pages/article")
				->set("article", $article)
				->bind("comments",$comments);
				
			
			
			$comments_url="comments/".$id;
			$comments=Request::factory($comments_url)->execute();
			$this->template->content=$content;
		}
	}
?>