<?php defined("SYSPATH") or die("No direct script access");
	
	class Controller_Admin_Main extends Controller_Common {
		
		public function before()
		{
			if(!Auth::instance()->logged_in("admin")) {
				$this->redirect("auth");
			}
			parent::before();
		}
		
		public function action_index() 
		{
			
			if($_POST) {
				$_POST = Arr::map('trim', $_POST);
				$writer=Arr::get($_POST,"writer");				
				$title=Arr::get($_POST,"title");
				$alt_title=Arr::get($_POST,"alt_title");
				$content_short=Arr::get($_POST,"content_short");
				$content_full=Arr::get($_POST,"content_full"); 
			 
				try
				{
					$add=ORM::factory('article');
					$add->writer=$writer;
					$add->title=$title;
					$add->alt_title=$alt_title;
					$add->date=date('y-m-d H:i:s');
					$add->content_short=$content_short;
					$add->content_full=$content_full;
					$add->save();
										
					$this->redirect("/");			
			 
				}
				catch(ORM_Validation_Exception $e)
				{
					$errors = $e->errors('article');
				}
			}
			
			$content=View::factory("/admin/show")
					->bind("errors",$errors);
			$this->template->content=$content;
		}
		
		public function action_edit($id='') 
		{		
			$id=$this->request->param("id");
			if($_POST) {
				$_POST = Arr::map('trim', $_POST);
				$writer=Arr::get($_POST,"writer");				
				$title=Arr::get($_POST,"title");
				$alt_title=Arr::get($_POST,"alt_title");
				$content_short=Arr::get($_POST,"content_short");
				$content_full=Arr::get($_POST,"content_full"); 
			 
				try
				{
					$add=ORM::factory('article')
						->where('id','=',$id)
						->find();
					$add->writer=$writer;
					$add->title=$title;
					$add->alt_title=$alt_title;
					$add->content_short=$content_short;
					$add->content_full=$content_full;
					$add->update();
										
					$this->redirect("/");			
			 
				}
				catch(ORM_Validation_Exception $e)
				{
					$errors = $e->errors('article');
				}
			}
			
			$article=ORM::factory('article')
					->where('id','=',$id)
					->find();
			
			$content=View::factory("/admin/edit")
					->bind("article",$article)
					->bind("errors",$errors);
			$this->template->content=$content;
		}
		
		public function action_delete($id='') 
		{
			$id=$this->request->param("id");
		
			ORM::factory('article')
					->where('id', '=', $id)
					->find()
					->delete();
					
			ORM::factory('comment')
					->where('article_id','=',$id)
					->find()
					->delete();
			
			$this->redirect('/list/listall');
		}
		
		public function action_delete_user()
		{
			$id=$this->request->param('id');
			
			$lists=ORM::factory('user')
				->where('id','=',$id)
				->find()
				->delete();
			
			$this->redirect('/list/users');
		}
		
		public function action_delete_comment($id='') 
		{
			$id=$this->request->param('id');

			ORM::factory('comment')
				->where('id','=',$id)
				->find()
				->delete();
				
			$this->redirect('/list/listall');
		}
	}