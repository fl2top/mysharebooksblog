<?php
	defined("SYSPATH") or die("No direct script access");
	
	class Controller_Page extends Controller_Common {
		
		public function action_index($id='') {
			$count = ORM::factory('article')->count_all();
            $pagination = Pagination::factory(array(
                'total_items' => $count,
            ))->route_params(array(
                'controller' => strtolower(Request::current()->controller()),
                'action' => strtolower(Request::current()->action()),
            ));
			$articles=ORM::factory('article')
					->limit($pagination->items_per_page)
					->offset($pagination->offset)
					->order_by('date', 'DESC')
					->find_all();
			
			$content=View::factory("/pages/show")
					->bind("articles",$articles)
					->bind("pagination",$pagination);
			
			$this->template->content=$content;
		}
		
	}
?>