<?php defined("SYSPATH") or die("No direct script access.");

class Model_Register extends ORM {
    public function rules()
    {
        return array(
            "username" => array(
                array("not_empty"),
                array("unique"),
            ),
            "email" => array(
                array("not_empty"),
                array("unique"),
            ),
            "password" => array(
                array("not_empty"),
            ),
            "password_confirm" => array(
                array('matches', array(':validation', 'password_confirm', 'password')),
            )
        );
    }
}