<?php defined("SYSPATH") or die("No direct script access.");

	class Model_Article extends ORM {
		
		protected $_has_many=array(
			"comments" => array(
				"model" => "comment",
				"foreign_key" => "article_id"
			)
		);
		
		public function rules() 
		{
			return array(
				"writer" => array(
						array("not_empty"),
				),
				"title" => array(
						array("not_empty"),
				),
				"alt_title" => array(
						array("not_empty"),
				),
				"content_short" => array(
						array("not_empty"),
				),
				"content_full" => array(
						array("not_empty")
				)
			);	
		}
	}